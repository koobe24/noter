package pl.koobe24.noter.loader;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import pl.koobe24.noter.bean.Note;
import pl.koobe24.noter.bean.User;
import pl.koobe24.noter.repository.NoteRepository;
import pl.koobe24.noter.repository.UserRepository;

import java.time.LocalDateTime;

/**
 * Created by tomas on 10.12.2016.
 */
@Component
@Slf4j
public class NoteLoader implements ApplicationListener<ContextRefreshedEvent>, Ordered {
    private final static int NUMBER_OF_NOTES = 10;
    private final static int DEFAULT_NOTE_TIME_LENGTH = 10;
    private NoteRepository noteRepository;
    private UserRepository userRepository;

    @Autowired
    public void setNoteRepository(NoteRepository noteRepository, UserRepository userRepository) {
        this.noteRepository = noteRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("Adding sample notes.");
        User user = userRepository.findOne(2l);
        for (int i = 0; i < NUMBER_OF_NOTES; i++) {
            LocalDateTime start = LocalDateTime.now().plusDays(i / 2);
            LocalDateTime end = start.plusMinutes((i + 1) * DEFAULT_NOTE_TIME_LENGTH);
            String content = "Notatka nr " + (i + 1) + ". Próba Springa itd.";
            String tags = "proba yolo jeszczeniewiem";
            Note note = new Note(start, end, content, tags, Note.NotePrivacy.PUBLIC, user);
            noteRepository.save(note);
            log.info("Saved Note - id: " + note.getId());
        }
        log.info("Adding sample notes completed.");
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
