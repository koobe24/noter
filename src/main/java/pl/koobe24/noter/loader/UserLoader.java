package pl.koobe24.noter.loader;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.koobe24.noter.bean.Role;
import pl.koobe24.noter.bean.User;
import pl.koobe24.noter.repository.UserRepository;

/**
 * Created by tomas on 12.12.2016.
 */
@Component
@Slf4j
public class UserLoader implements ApplicationListener<ContextRefreshedEvent>, Ordered {
    private final static int NUMBER_OF_USERS = 5;
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("Adding sample users.");
        User admin = new User("admin", new BCryptPasswordEncoder().encode("admin"), Role.ADMIN, User.UserPrivacy.PUBLIC);
        userRepository.save(admin);
        for (int i = 0; i < NUMBER_OF_USERS; i++) {
            String mail = "sample" + i + "@gmail.com";
            String passwordHash = new BCryptPasswordEncoder().encode("password" + i);
            Role role = Role.USER;
            User user = new User(mail, passwordHash, role, i % 2 == 0 ? User.UserPrivacy.PUBLIC : User.UserPrivacy.PRIVATE_INVISIBLE);
            userRepository.save(user);
            log.info("Saved user - id: " + user.getId() + " " + user.getLogin() + " password" + i);
        }
        log.info("Adding sample users completed.");
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
