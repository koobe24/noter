package pl.koobe24.noter.editor;

import java.beans.PropertyEditorSupport;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by tomas on 11.12.2016.
 */
public class LocalDateTimeEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        return getValue() != null ? ((LocalDateTime) getValue()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) : "";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        setValue(LocalDateTime.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
    }
}
