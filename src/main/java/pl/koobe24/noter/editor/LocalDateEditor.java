package pl.koobe24.noter.editor;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by tomas on 11.12.2016.
 */
public class LocalDateEditor extends PropertyEditorSupport {
    @Override
    public String getAsText() {
        return getValue() != null ? ((LocalDate) getValue()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) : "";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        setValue(LocalDate.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }
}
