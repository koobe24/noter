package pl.koobe24.noter.service;

import pl.koobe24.noter.bean.Role;
import pl.koobe24.noter.bean.User;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by tomasz on 20.12.16.
 */
public interface UserService {
    Optional<User> getUserById(long id);

    Optional<User> getUserByLogin(String login);

    Collection<User> getAllVisibleUsers(Role currentUserRole);

    Optional<User> findOneAvailable(long id,Role currentUserRole);
    //todo create
}
