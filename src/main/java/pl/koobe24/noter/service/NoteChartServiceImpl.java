package pl.koobe24.noter.service;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Ordering;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.koobe24.noter.authorizing.CurrentUser;
import pl.koobe24.noter.bean.Note;
import pl.koobe24.noter.bean.User;
import pl.koobe24.noter.repository.NoteRepository;

import java.time.LocalDate;

/**
 * Created by tomasz on 17.12.16.
 */
@Service
public class NoteChartServiceImpl implements NoteChartService {
    @Autowired
    private NoteRepository noteRepository;

    public void setNoteRepository(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @Override
    public Multimap<String, Note> notesByTags() {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        Iterable<Note> notes = noteRepository.findByUser(user);
        return notesByTags(notes);
    }

    @Override
    public Multimap<String, Note> notesByTags(Iterable<Note> notes) {
        Multimap<String, Note> notesByTags = MultimapBuilder.hashKeys().treeSetValues(Ordering.natural().reverse()).build();
        notes.forEach(n -> {
            for (String tag : n.getTags().split(" ")) {
                notesByTags.put(tag, n);
            }
        });
        return notesByTags;
    }

    @Override
    public Multimap<String, Note> notesByTagsForDay(LocalDate localDate) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        Iterable<Note> notes = noteRepository.findForDayForOwner(localDate, user);
        Multimap<String, Note> notesByTags = MultimapBuilder.hashKeys().treeSetValues(Ordering.natural().reverse()).build();
        notes.forEach(n -> {
            for (String tag : n.getTags().split(" ")) {
                notesByTags.put(tag, n);
            }
        });
        return notesByTags;
    }

}
