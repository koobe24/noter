package pl.koobe24.noter.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.koobe24.noter.bean.Role;
import pl.koobe24.noter.bean.User;
import pl.koobe24.noter.repository.UserRepository;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by tomasz on 20.12.16.
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    //to do logowania
    @Override
    public Optional<User> getUserById(long id) {
        log.info("get user by id");
        return Optional.ofNullable(userRepository.findOne(id));
    }

    //to do logowania
    @Override
    public Optional<User> getUserByLogin(String login) {
        log.info("get user by login");
        log.info(userRepository.findOneByLogin(login).isPresent() + "");
        return userRepository.findOneByLogin(login);
    }

    @Override
    public Collection<User> getAllVisibleUsers(Role currentUserRole) {
        Collection<User> users;
        if (currentUserRole == Role.ADMIN) {
            users = userRepository.findAll();
        } else {
            users = userRepository.findAllByPrivacy(User.UserPrivacy.PUBLIC);
        }
        return users;
    }

    @Override
    public Optional<User> findOneAvailable(long id, Role currentUserRole) {
        Optional<User> user;
        if (currentUserRole == Role.ADMIN) {
            user = Optional.ofNullable(userRepository.findOneWithNotes(id));
        } else {
            user = Optional.ofNullable(userRepository.findOneByIdAndPrivacy(id, User.UserPrivacy.PUBLIC));
        }
        return user;
    }
}
