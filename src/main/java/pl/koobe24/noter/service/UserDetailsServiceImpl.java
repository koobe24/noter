package pl.koobe24.noter.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import pl.koobe24.noter.authorizing.CurrentUser;
import pl.koobe24.noter.bean.User;

/**
 * Created by tomasz on 20.12.16.
 */
@Component
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserService userService;

    @Autowired
    public UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Load user by username.");
        User user = userService.getUserByLogin(username).orElseThrow(() ->
                new UsernameNotFoundException("Username was not found"));
        //todo lokalizacja
        return new CurrentUser(user);
    }
}
