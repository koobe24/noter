package pl.koobe24.noter.service;

import com.google.common.collect.Multimap;
import org.springframework.stereotype.Service;
import pl.koobe24.noter.bean.Note;

import java.time.LocalDate;

/**
 * Created by tomasz on 17.12.16.
 */
public interface NoteChartService {
    Multimap<String, Note> notesByTags();

    Multimap<String, Note> notesByTagsForDay(LocalDate localDate);

    Multimap<String, Note> notesByTags(Iterable<Note> notes);

}
