package pl.koobe24.noter.constraint;

import pl.koobe24.noter.validator.StartEndValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by tomasz on 18.12.16.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StartEndValidator.class)
public @interface StartEnd {
    String message() default "{validation.start_end}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
