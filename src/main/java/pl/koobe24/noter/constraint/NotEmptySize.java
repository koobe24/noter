package pl.koobe24.noter.constraint;

import pl.koobe24.noter.validator.NotEmptySizeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by tomasz on 20.12.16.
 */
@Constraint(validatedBy = NotEmptySizeValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotEmptySize {

    String message() default "{validation.size_if_not_empty}";

    int minSize() default 0;

    int maxSize() default Integer.MAX_VALUE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
