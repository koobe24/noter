package pl.koobe24.noter.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by tomas on 10.12.2016.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Note implements Comparable<Note> {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private LocalDateTime start;

    @Column
    private LocalDateTime end;

    @Column
    private String noteContent;

    @Column
    private String tags;

    @Column
    @Enumerated(EnumType.STRING)
    private NotePrivacy notePrivacy;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    public Note(LocalDateTime start, LocalDateTime end, String noteContent, String tags, NotePrivacy notePrivacy, User user) {
        this.start = start;
        this.end = end;
        this.noteContent = noteContent;
        this.tags = tags;
        this.notePrivacy = notePrivacy;
        this.user = user;
    }

    public Note(LocalDateTime start, LocalDateTime end, String noteContent, String tags, NotePrivacy notePrivacy) {
        this.start = start;
        this.end = end;
        this.noteContent = noteContent;
        this.tags = tags;
        this.notePrivacy = notePrivacy;
    }

    public long getMinutes() {
        return ChronoUnit.MINUTES.between(start, end);
    }

    @Override
    public int compareTo(Note o) {
        return start.compareTo(o.getStart());
    }

    public enum NotePrivacy {
        PRIVATE_INVISIBLE("note_privacy.private_invisible"),
        PRIVATE_VISIBLE("note_privacy.private_visible"),
        PUBLIC("note_privacy.public");

        private final String displayName;


        NotePrivacy(String displayName) {
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return displayName;
        }
    }
}
