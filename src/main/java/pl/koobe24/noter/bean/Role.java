package pl.koobe24.noter.bean;

/**
 * Created by tomas on 12.12.2016.
 */
public enum Role {
    USER, ADMIN
}
