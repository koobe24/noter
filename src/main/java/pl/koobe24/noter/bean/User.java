package pl.koobe24.noter.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by tomas on 12.12.2016.
 */
@Entity
@NoArgsConstructor
@Data
public class User {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    private String passwordHash;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany
    @JoinColumn(name = "user_id")
    private Collection<Note> notes;

    @Column
    @Enumerated(EnumType.STRING)
    private UserPrivacy privacy;

    public User(String login, String passwordHash, Role role, UserPrivacy privacy) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
        this.privacy = privacy;
    }

    public enum UserPrivacy {
        PRIVATE_INVISIBLE("user_privacy.private_invisible"),
        PRIVATE_VISIBLE("user_privacy.private_visible"),
        PUBLIC("user_privacy.public");

        private final String displayName;

        UserPrivacy(String displayName) {
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return displayName;
        }
    }

}
