package pl.koobe24.noter.controller;

import com.google.common.collect.Multimap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.koobe24.noter.bean.Note;
import pl.koobe24.noter.service.NoteChartService;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by tomasz on 18.12.16.
 */
@Controller
@RequestMapping("/statistics")
@PreAuthorize("hasRole('ROLE_USER')")
@Slf4j
public class StatisticsController {

    @Autowired
    private NoteChartService chartService;

    private void setChartService(NoteChartService chartService) {
        this.chartService = chartService;
    }

    @RequestMapping
    public String mainStatistics(Model model) {
        Multimap<String, Note> notesByTags = chartService.notesByTags();
        Map<String, Long> minutesForTags = new TreeMap<>();
        for (String tag : notesByTags.keySet()) {
            long sum = 0;
            for (Note n : notesByTags.get(tag)) {
                sum += n.getMinutes();
            }
            minutesForTags.put(tag, sum);
        }
        model.addAttribute("chart_data", minutesForTags);
        return "statistics";
    }
}
