package pl.koobe24.noter.controller;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pl.koobe24.noter.authorizing.CurrentUser;
import pl.koobe24.noter.bean.Note;
import pl.koobe24.noter.bean.User;
import pl.koobe24.noter.editor.LocalDateEditor;
import pl.koobe24.noter.editor.LocalDateTimeEditor;
import pl.koobe24.noter.form.NoteForm;
import pl.koobe24.noter.repository.NoteRepository;
import pl.koobe24.noter.util.DaysToFile;
import pl.koobe24.util.guava.MultimapCollectors;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by tomas on 16.12.2016.
 */
@Controller
@RequestMapping("/notes")
@Slf4j
public class NotesController {

    private NoteRepository noteRepository;

    @Autowired
    public void setNoteRepository(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @RequestMapping()
    public String allNotes(Model model) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        List<Note> notes = (List<Note>) noteRepository.findByUser(user);
        Multimap<LocalDate, Note> multimap = notes.stream().collect(MultimapCollectors.listMultimap((Note note) -> note.getStart().toLocalDate()));
        model.addAttribute("notesMap", multimap);
        log.info("Returned " + multimap.size() + " notes in " + multimap.keySet().size() + " groups.");
        return "notes";
    }

    @RequestMapping("/today")
    public String todaysNotes(Model model) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        List<Note> notes = noteRepository.findTodaysForOwner(user);
        Multimap<LocalDate, Note> multimap = notes.stream().collect(MultimapCollectors.listMultimap((Note note) -> note.getStart().toLocalDate()));
        model.addAttribute("notesMap", multimap);
        return "notes";
    }

    @RequestMapping("/new")
    public String newNote(Model model) {
        model.addAttribute("noteForm", new NoteForm());
        return "new_note";
    }

    @PostMapping("/new")
    public String submitNewNote(@Valid NoteForm noteForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(System.out::println);
            return "new_note";
        }
        Note note = noteForm.getAsNote();
        if (note.getUser() == null) {
            CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            User user = currentUser.getUser();
            note.setUser(user);
        }
        noteRepository.save(note);
        return "redirect:/notes";
    }

    @RequestMapping("/edit/{id}")
    public String editNote(@PathVariable long id, Model model) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        NoteForm noteForm = new NoteForm(noteRepository.findOneForOwner(id, user));
        log.info("id noteform " + noteForm.getId());
        model.addAttribute("noteForm", noteForm);
        return "new_note";
    }

    @RequestMapping("/delete/{id}")
    public String deleteNote(@PathVariable long id) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        noteRepository.deleteByOwner(id, user);
        return "redirect:/notes";
    }

    @RequestMapping("/to_file")
    @ResponseBody
    public void getFileForDate(String date, HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=days.txt");
        LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        List<Note> notes = noteRepository.findForDayForOwner(localDate, user);
        Multimap<LocalDate, Note> multimap = notes.stream().collect(MultimapCollectors.listMultimap((Note note) -> note.getStart().toLocalDate()));
        DaysToFile daysToFile = new DaysToFile(multimap);
        try {
            IOUtils.copy(new ByteArrayInputStream(daysToFile.formatDaysToString().getBytes()), response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/{id}")
    public String viewNote(@PathVariable long id, Model model) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        Note note = noteRepository.findOneForOwner(id, user);
        model.addAttribute("note", note);
        return "view_note";
    }

    @RequestMapping("/tag/{tag}")
    public String viewNotesForTag(@PathVariable String tag, Model model) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        List<Note> notes = noteRepository.findForTagForOwner(tag, user);
        Multimap<LocalDate, Note> multimap = notes.stream().collect(MultimapCollectors.listMultimap((Note note) -> note.getStart().toLocalDate()));
        model.addAttribute("notesMap", multimap);
        log.info("Returned " + multimap.size() + " notes in " + multimap.keySet().size() + " groups.");
        return "notes";
    }

    @RequestMapping("/for_day/{date}")
    public String viewNotesForDay(@PathVariable String date, Model model) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        List<Note> notes = noteRepository.findForDayForOwner(localDate, user);
        Multimap<LocalDate, Note> multimap = notes.stream().collect(MultimapCollectors.listMultimap((Note note) -> note.getStart().toLocalDate()));
        model.addAttribute("notesMap", multimap);
        log.info("Returned " + multimap.size() + " notes in " + multimap.keySet().size() + " groups.");
        return "notes";
    }

    @InitBinder
    public void bindingPreparation(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDateTime.class, new LocalDateTimeEditor());
        binder.registerCustomEditor(LocalDate.class, new LocalDateEditor());
    }
}
