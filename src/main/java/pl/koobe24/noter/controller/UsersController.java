package pl.koobe24.noter.controller;

import com.google.common.collect.Multimap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.koobe24.noter.authorizing.CurrentUser;
import pl.koobe24.noter.bean.Note;
import pl.koobe24.noter.bean.User;
import pl.koobe24.noter.service.UserService;
import pl.koobe24.util.guava.MultimapCollectors;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by tomas on 12.12.2016.
 */
@Controller
@RequestMapping("/users")
@PreAuthorize("hasRole('ROLE_USER')")
@Slf4j
public class UsersController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping
    public String allUsers(Model model) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = currentUser.getUser();
        Collection<User> users = userService.getAllVisibleUsers(user.getRole());
        model.addAttribute("users", users);
        return "users";
    }

    @RequestMapping("/{id}")
    public String viewUser(@PathVariable long id, Model model) {
        CurrentUser currentUser = (CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> user = userService.findOneAvailable(id, currentUser.getUser().getRole());
        if (user.isPresent()) {
            log.info(user.get().getNotes().size() + " user notes size");
            Multimap<LocalDate, Note> notes = user.get().getNotes().stream()
                    .filter(note -> note.getNotePrivacy() == Note.NotePrivacy.PUBLIC)
                    .collect(MultimapCollectors.listMultimap((Note note) -> note.getStart().toLocalDate()));
            model.addAttribute("notes", notes);
            model.addAttribute("user", user.get());
            return "view_user";
        } else {
            //todo zmien potem
            return "redirect:/";
        }
    }
}
