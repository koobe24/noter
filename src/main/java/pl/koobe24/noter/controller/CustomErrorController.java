package pl.koobe24.noter.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by tomasz on 18.12.16.
 */
public class CustomErrorController implements ErrorController {

    private final static String errorPath = "/error";

    //todo customizacja
    @RequestMapping(value = errorPath)
    public String error() {
        return "error";
    }

    @Override
    public String getErrorPath() {
        return null;
    }
}
