package pl.koobe24.noter.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

/**
 * Created by tomas on 10.12.2016.
 */
@Controller
@Slf4j
public class MainController {

    @RequestMapping("/")
    public String mainPage(Model model) {
        model.addAttribute("message", "Welcome home");
        return "home";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage(@RequestParam Optional<String> error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

}
