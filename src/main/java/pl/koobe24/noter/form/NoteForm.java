package pl.koobe24.noter.form;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import pl.koobe24.noter.bean.Note;
import pl.koobe24.noter.bean.User;
import pl.koobe24.noter.constraint.NotEmptySize;
import pl.koobe24.noter.constraint.StartEnd;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by tomasz on 20.12.16.
 */
@Data
@NoArgsConstructor
@StartEnd
public class NoteForm {
    @NotBlank
    private String start;
    @NotBlank
    private String end;
    @NotBlank
    @Size(min = 3, max = 1000, message = "{validation.size}")
    private String noteContent;
    @NotEmptySize(minSize = 2, maxSize = 1000)
    private String tags;
    @NotNull
    private Note.NotePrivacy notePrivacy;

    private long id;

    private User user;

    private DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public NoteForm(Note n) {
        start = n.getStart().format(format);
        end = n.getEnd().format(format);
        noteContent = n.getNoteContent();
        tags = n.getTags();
        id = n.getId();
        notePrivacy = n.getNotePrivacy();
        user = n.getUser();
    }

    public LocalDateTime getStartAsLocalDateTime() {
        return LocalDateTime.parse(start, format);
    }

    public LocalDateTime getEndAsLocalDateTime() {
        return LocalDateTime.parse(end, format);
    }

    public Note getAsNote() {
        return new Note(id, getStartAsLocalDateTime(), getEndAsLocalDateTime(), noteContent, tags, notePrivacy, user);
    }
}
