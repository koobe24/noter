package pl.koobe24.noter.util;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Ordering;
import lombok.extern.slf4j.Slf4j;
import pl.koobe24.noter.bean.Note;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

/**
 * Created by tomas on 11.12.2016.
 */
@Slf4j
public class DaysToFile {
    private Multimap<LocalDate, Note> notesForDays;

    public DaysToFile(Multimap<LocalDate, Note> notesForDays) {
        sortNotes(notesForDays);
    }

    public String formatDaysToString() {
        StringBuilder builder = new StringBuilder();
        notesForDays.keySet().forEach(day -> builder.append(dayToString(day)));
        return builder.toString();
    }

    private void sortNotes(Multimap<LocalDate, Note> notesForDays) {
        this.notesForDays = MultimapBuilder.treeKeys().treeSetValues(Ordering.natural().reverse()).build(notesForDays);
    }

    private String dayToString(LocalDate date) {
        StringBuilder builder = new StringBuilder();
        builder.append(date.format(DateTimeFormatter.ofPattern("EEEE dd.MM.yyyy")));
        builder.append(System.getProperty("line.separator"));
        notesForDays.get(date).forEach(note -> builder.append(noteToString(note)));
        builder.append(System.getProperty("line.separator"));
        return builder.toString();
    }

    private String noteToString(Note note) {
        StringBuilder builder = new StringBuilder();
        builder.append(note.getStart().toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm")));
        builder.append(" - ");
        builder.append(note.getEnd().toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm")));
        builder.append(System.getProperty("line.separator"));
        builder.append(note.getNoteContent());
        builder.append(System.getProperty("line.separator"));
        return builder.toString();
    }

}
