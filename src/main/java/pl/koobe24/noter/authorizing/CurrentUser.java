package pl.koobe24.noter.authorizing;


import org.springframework.security.core.authority.AuthorityUtils;
import pl.koobe24.noter.bean.Role;
import pl.koobe24.noter.bean.User;

/**
 * Created by tomasz on 20.12.16.
 */
public class CurrentUser extends org.springframework.security.core.userdetails.User {
    private User user;

    public CurrentUser(User user) {
        super(user.getLogin(), user.getPasswordHash(), AuthorityUtils.createAuthorityList(user.getRole().toString()));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Long getId() {
        return user.getId();
    }

    public Role getRole() {
        return user.getRole();
    }
}
