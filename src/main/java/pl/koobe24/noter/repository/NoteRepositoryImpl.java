package pl.koobe24.noter.repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.koobe24.noter.bean.Note;
import pl.koobe24.noter.bean.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by tomas on 10.12.2016.
 */
@Transactional(readOnly = true)
@Slf4j
public class NoteRepositoryImpl implements NoteRepositoryCustom {

    @PersistenceContext
    private EntityManager manager;


    @Override
    public List<Note> findTodaysForOwner(User user) {
        log.info("Find todays notes.");
        return findForDayForOwner(LocalDate.now(), user);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Note> findForDayForOwner(LocalDate day, User user) {
        log.info("Find notes for " + day.toString());
        LocalDateTime dayStart = day.atStartOfDay();
        LocalDateTime dayEnd = day.atTime(23, 59, 59);
        Query query = manager.createQuery("SELECT n FROM Note n WHERE n.start BETWEEN ?1 AND ?2 AND n.user = ?3");
        query.setParameter(1, dayStart);
        query.setParameter(2, dayEnd);
        query.setParameter(3, user);
        return query.getResultList();
    }

    @Override
    public List<Note> findForTags(List<String> tag) {
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Note> findForTagForOwner(String tag, User user) {
        log.info("Find notes for tag: " + tag);
        Query query = manager.createQuery("SELECT n FROM Note n WHERE n.tags like concat('%',?1,'%') AND n.user = ?2");
        query.setParameter(1, tag);
        query.setParameter(2, user);
        return query.getResultList();
    }

    @Override
    public Note findOneForOwner(long id, User user) {
        Query query = manager.createQuery("SELECT n FROM Note n WHERE n.id=?1 AND n.user=?2");
        query.setParameter(1, id);
        query.setParameter(2, user);
        return (Note) query.getSingleResult();
    }

    @Override
    public Long deleteByOwner(long id, User user) {
        Query query = manager.createQuery("DELETE FROM Note n where n.id=?1 AND n.user=?2");
        query.setParameter(1, id);
        query.setParameter(2, id);
        return (long) query.executeUpdate();
    }

}
