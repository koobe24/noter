package pl.koobe24.noter.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.koobe24.noter.bean.Role;
import pl.koobe24.noter.bean.User;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by tomas on 12.12.2016.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findOneByLogin(String login);

    Collection<User> findAllByPrivacy(User.UserPrivacy privacy);

    Collection<User> findAll();

    User findOneByIdAndPrivacy(long id, User.UserPrivacy privacy);

    @Query("SELECT u from User u left join fetch u.notes where u.id = (:id)")
    User findOneWithNotes(@Param("id") long id);

    @Query("SELECT u from User u join fetch u.notes where u.id = (:id) and u.role=(:role)")
    User findOneAvailableWithNotes(@Param("id") long id, @Param("role") Role role);
}
