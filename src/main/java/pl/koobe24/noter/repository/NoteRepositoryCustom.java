package pl.koobe24.noter.repository;

import pl.koobe24.noter.bean.Note;
import pl.koobe24.noter.bean.User;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by tomas on 10.12.2016.
 */
public interface NoteRepositoryCustom {
    List<Note> findTodaysForOwner(User user);

    List<Note> findForDayForOwner(LocalDate day, User user);

    List<Note> findForTags(List<String> tag);

    List<Note> findForTagForOwner(String tag, User user);

    Note findOneForOwner(long id, User user);

    Long deleteByOwner(long id, User user);

}
