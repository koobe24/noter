package pl.koobe24.noter.repository;

import org.springframework.data.repository.CrudRepository;
import pl.koobe24.noter.bean.Note;
import pl.koobe24.noter.bean.User;

import java.util.Collection;

/**
 * Created by tomas on 10.12.2016.
 */
public interface NoteRepository extends CrudRepository<Note, Long>, NoteRepositoryCustom {
    Collection<Note> findByUser(User user);
}
