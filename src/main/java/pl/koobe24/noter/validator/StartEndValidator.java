package pl.koobe24.noter.validator;

import pl.koobe24.noter.constraint.StartEnd;
import pl.koobe24.noter.form.NoteForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by tomasz on 18.12.16.
 */
public class StartEndValidator implements ConstraintValidator<StartEnd, NoteForm> {


    @Override
    public void initialize(StartEnd startEnd) {

    }

    @Override
    public boolean isValid(NoteForm noteForm, ConstraintValidatorContext constraintValidatorContext) {
        LocalDateTime startTime;
        LocalDateTime endTime;
        boolean isValid = false;
        //todo zamiana na bez negacji
        if (!noteForm.getStart().isEmpty() && !noteForm.getEnd().isEmpty()) {
            try {
                startTime = noteForm.getStartAsLocalDateTime();
                endTime = noteForm.getEndAsLocalDateTime();
                if (startTime.isBefore(endTime)) {
                    isValid = true;
                }
            } catch (Exception e) {
                return false;
            }
        }
        return isValid;
    }
}
