package pl.koobe24.noter.validator;

import pl.koobe24.noter.constraint.NotEmptySize;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by tomasz on 20.12.16.
 */
public class NotEmptySizeValidator implements ConstraintValidator<NotEmptySize, String> {
    private NotEmptySize notEmptySize;

    @Override
    public void initialize(NotEmptySize notEmptySize) {
        this.notEmptySize = notEmptySize;
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null || s.isEmpty()) {
            return true;
        }
        return s.length() >= notEmptySize.minSize() && s.length() <= notEmptySize.maxSize();
    }
}
