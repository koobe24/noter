package pl.koobe24.noter.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by tomas on 10.12.2016.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan("pl.koobe24.noter")
@EntityScan(basePackages = {"pl.koobe24.noter.bean"})
@EnableJpaRepositories(basePackages = {"pl.koobe24.noter.repository"})
@EnableTransactionManagement
public class RepositoryConfiguration {

}
