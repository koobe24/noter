package pl.koobe24.util.guava;


import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 * Created by tomasz on 22.12.16.
 */
public class MultimapCollectors {

    public static <K, T> ListMultimapCollector<K, T> listMultimap(Function<T, K> keyFunction) {
        return new ListMultimapCollector<K, T>(keyFunction);
    }

    public static <K, T> HashSetMultimapCollector<K, T> setMultimap(Function<T, K> keyFunction) {
        return new HashSetMultimapCollector<K, T>(keyFunction);
    }

    public static <K, T> LinkedListMultimapCollector<K, T> linkedListMultimap(Function<T, K> keyFunction) {
        return new LinkedListMultimapCollector<K, T>(keyFunction);
    }


    private static abstract class MultimapCollector<K, T, A extends Multimap<K, T>, R extends Multimap<K, T>> implements Collector {
        private Function<T, K> keyFunction;

        public MultimapCollector(Function<T, K> function) {
            Preconditions.checkNotNull(function, "Key function can't be null");
            keyFunction = function;
        }

        @Override
        public BiConsumer<A, T> accumulator() {
            return (map, value) -> map.put(keyFunction.apply(value), value);
        }

        @Override
        public BinaryOperator<A> combiner() {
            return (m1, m2) -> {
                m1.putAll(m2);
                return m1;
            };
        }

        @Override
        public Function<A, R> finisher() {
            return map -> (R) map;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return Collections.unmodifiableSet(EnumSet.of(Characteristics.IDENTITY_FINISH));
        }
    }

    private static class ListMultimapCollector<K, T> extends MultimapCollector {
        private ListMultimapCollector(Function<T, K> keyFunction) {
            super(keyFunction);
        }

        @Override
        public Supplier<ArrayListMultimap<K, T>> supplier() {
            return ArrayListMultimap::create;
        }
    }

    private static class HashSetMultimapCollector<K, T> extends MultimapCollector {
        private HashSetMultimapCollector(Function<T, K> keyFunction) {
            super(keyFunction);
        }

        @Override
        public Supplier<HashMultimap<K, T>> supplier() {
            return HashMultimap::create;
        }
    }

    private static class LinkedListMultimapCollector<K, T> extends MultimapCollector {
        private LinkedListMultimapCollector(Function<T, K> keyFunction) {
            super(keyFunction);
        }

        @Override
        public Supplier<LinkedListMultimap<K, T>> supplier() {
            return LinkedListMultimap::create;
        }
    }
}

