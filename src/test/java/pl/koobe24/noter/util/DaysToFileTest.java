package pl.koobe24.noter.util;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.junit.Before;
import org.junit.Test;
import pl.koobe24.noter.bean.Note;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

/**
 * Created by tomas on 16.12.2016.
 */
public class DaysToFileTest {

    private Multimap<LocalDate, Note> notesForDays;
    private DaysToFile dtf;
    private static LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(2016, Month.DECEMBER, 16, 10, 0, 0);
    private static int NO_OF_DAYS = 3;

    @Before
    public void beforeMethod() {
        notesForDays = ArrayListMultimap.create();
    }

    @Test
    public void isOutputStringEmptyWhenNoDays() {
        dtf = new DaysToFile(notesForDays);
        String output = dtf.formatDaysToString();
        assertThat(output, isEmptyString());
    }

    @Test
    public void isOutputStringNotEmptyWhenNoteIsCurrent() {
        Note note = new Note(LOCAL_DATE_TIME, LOCAL_DATE_TIME.plusMinutes(60), "First line.\nSecond line.", "", Note.NotePrivacy.PUBLIC);
        notesForDays.put(LOCAL_DATE_TIME.toLocalDate(), note);
        dtf = new DaysToFile(notesForDays);
        String output = dtf.formatDaysToString();
        assertThat(output, not(isEmptyString()));
    }

    @Test
    public void isOutputStringCorrectlyFormattedWhenOneNoteForOneDay() {
        Note note = new Note(LOCAL_DATE_TIME, LOCAL_DATE_TIME.plusMinutes(60), "First line." + System.getProperty("line.separator") + "Second line.", "", Note.NotePrivacy.PUBLIC);
        notesForDays.put(LOCAL_DATE_TIME.toLocalDate(), note);
        dtf = new DaysToFile(notesForDays);
        String[] output = dtf.formatDaysToString().split(System.getProperty("line.separator"));
        assertThat(output.length, is(4));
        assertThat(output[0], is("piątek 16.12.2016"));
        assertThat(output[1], is("10:00 - 11:00"));
        assertThat(output[2], is("First line."));
        assertThat(output[3], is("Second line."));
    }

    @Test
    public void isOutputStringCorrectlyFormattedWhenTwoNotesForOneDay() {
        Note note = new Note(LOCAL_DATE_TIME, LOCAL_DATE_TIME.plusMinutes(60), "First line." + System.getProperty("line.separator") + "Second line.", "", Note.NotePrivacy.PUBLIC);
        Note secondNote = new Note(LOCAL_DATE_TIME.plusMinutes(60), LOCAL_DATE_TIME.plusMinutes(120), "First line." + System.getProperty("line.separator") + "Second line."
                + System.getProperty("line.separator") + "Third line.", "", Note.NotePrivacy.PUBLIC);
        notesForDays.putAll(LOCAL_DATE_TIME.toLocalDate(), Arrays.asList(note, secondNote));
        dtf = new DaysToFile(notesForDays);
        String[] output = dtf.formatDaysToString().split(System.getProperty("line.separator"));
        assertThat(output.length, is(8));
        assertThat(output[0], is("piątek 16.12.2016"));
        assertThat(output[1], is("11:00 - 12:00"));
        assertThat(output[2], is("First line."));
        assertThat(output[3], is("Second line."));
        assertThat(output[4], is("Third line."));
        assertThat(output[5], is("10:00 - 11:00"));
        assertThat(output[6], is("First line."));
        assertThat(output[7], is("Second line."));
    }

    @Test
    public void areNotesOrderedDescendingInDay() {
        Note note = new Note(LOCAL_DATE_TIME, LOCAL_DATE_TIME.plusMinutes(60), "First line.", "", Note.NotePrivacy.PUBLIC);
        Note secondNote = new Note(LOCAL_DATE_TIME.plusMinutes(60), LOCAL_DATE_TIME.plusMinutes(120), "First line.", "", Note.NotePrivacy.PUBLIC);
        Note thirdNote = new Note(LOCAL_DATE_TIME.plusMinutes(-60), LOCAL_DATE_TIME, "First line.", "", Note.NotePrivacy.PUBLIC);
        notesForDays.putAll(LOCAL_DATE_TIME.toLocalDate(), Arrays.asList(note, secondNote, thirdNote));
        dtf = new DaysToFile(notesForDays);
        String[] output = dtf.formatDaysToString().split(System.getProperty("line.separator"));
        assertThat(output[1], is("11:00 - 12:00"));
        assertThat(output[3], is("10:00 - 11:00"));
        assertThat(output[5], is("09:00 - 10:00"));
    }

    @Test
    public void isOutputStringCorrectlyFormattedWhenOneNoteForEachDay() {
        for (int i = 0; i < NO_OF_DAYS; i++) {
            LocalDateTime day = LOCAL_DATE_TIME.plusDays(i);
            Note n = new Note(day, day.plusMinutes(60), "First line.", "", Note.NotePrivacy.PUBLIC);
            notesForDays.put(day.toLocalDate(), n);
        }
        dtf = new DaysToFile(notesForDays);
        String[] output = dtf.formatDaysToString().split(System.getProperty("line.separator"));
        assertThat(output[0], is("piątek 16.12.2016"));
        assertThat(output[1], is("10:00 - 11:00"));
        assertThat(output[2], is("First line."));
        assertThat(output[4],is("sobota 17.12.2016"));
        assertThat(output[5], is("10:00 - 11:00"));
        assertThat(output[6], is("First line."));
        assertThat(output[8],is("niedziela 18.12.2016"));
        assertThat(output[9], is("10:00 - 11:00"));
        assertThat(output[10], is("First line."));
    }
}
